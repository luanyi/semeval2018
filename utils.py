import os
import re
import codecs
import numpy as np
import theano
import pdb
import time

from sklearn.metrics import classification_report
import random
from collections import defaultdict
models_path = "./models"
eval_path = "./evaluation"
# true_path = './evaluation/relations/dev1.relation'
#eval_temp = os.path.join(eval_path, "temp")
eval_script = os.path.join(eval_path, "semeval2018_task7_scorer-v1.2.pl")

def report2dict(cr):
        # Parse rows
    tmp = list()
    for row in cr.split("\n"):
        parsed_row = [x for x in row.split("  ") if len(x) > 0]
        if len(parsed_row) > 0:
            tmp.append(parsed_row)
            
    # Store in dictionary
    measures = tmp[0]
            
    D_class_data = defaultdict(dict)
    for row in tmp[1:]:
        class_label = row[0]
        for j, m in enumerate(measures):
            D_class_data[class_label][m.strip()] = float(row[j + 1].strip())
    return D_class_data
                                                                                                    
def get_name(parameters):
    """
    Generate a model name from its parameters.
    """
    l = []
    for k, v in parameters.items():
        if k == 'outdir':continue
        if k == 'tag_scheme':continue
        if k == 'crf':continue
        if k == 'lower':continue
        if k == 'isdep':continue
        if k == 'relation':continue
        if k == 'train_true':continue
        if k == 'zeros':continue
        if k == 'inbetween_path':continue
        if 'reload' in k:
            if v:
                v = str(1) 
        if 'pre_emb' in k: continue
        if 'bidirect' in k:continue
        if type(v) is str and "/" in v:
            l.append((k, v[::-1][:v[::-1].index('/')][::-1]))
        else:
            l.append((k, v))
    date = time.strftime("%Y%m%d%H")
    name = ",".join(["%s=%s" % (k, str(v).replace(',', '')) for k, v in l])
    return "".join(i for i in name if i not in "\/:*?<>|")+','+date + str(os.getpid())


def set_values(name, param, pretrained):
    """
    Initialize a network parameter with pretrained values.
    We check that sizes are compatible.
    """
    param_value = param.get_value()
    if pretrained.size != param_value.size:
        raise Exception(
            "Size mismatch for parameter %s. Expected %i, found %i."
            % (name, param_value.size, pretrained.size)
        )
    param.set_value(np.reshape(
        pretrained, param_value.shape
    ).astype(np.float32))


def shared(shape, name):
    """
    Create a shared object of a numpy array.
    """
    if len(shape) == 1:
        value = np.zeros(shape)  # bias are initialized with zeros
    else:
        drange = np.sqrt(6. / (np.sum(shape)))
        value = drange * np.random.uniform(low=-1.0, high=1.0, size=shape)
    return theano.shared(value=value.astype(theano.config.floatX), name=name)


def create_dico(item_list):
    """
    Create a dictionary of items from a list of list of items.
    """
    assert type(item_list) is list
    dico = {}
    for items in item_list:
        for item in items:
            if item not in dico:
                dico[item] = 1
            else:
                dico[item] += 1
    return dico


def create_mapping(dico):
    """
    Create a mapping (item to ID / ID to item) from a dictionary.
    Items are ordered by decreasing frequency.
    """
    sorted_items = sorted(dico.items(), key=lambda x: (-x[1], x[0]))
    id_to_item = {i: v[0] for i, v in enumerate(sorted_items)}
    item_to_id = {v: k for k, v in id_to_item.items()}
    return item_to_id, id_to_item


def zero_digits(s):
    """
    Replace every digit in a string by a zero.
    """
    return re.sub('\d', '0', s)


def iob2(tags):
    """
    Check that tags have a valid IOB format.
    Tags in IOB1 format are converted to IOB2.
    """
    for i, tag in enumerate(tags):
        if tag == 'O':
            continue
        split = tag.split('-')
        if len(split) != 2 or split[0] not in ['I', 'B']:
            return False
        if split[0] == 'B':
            continue
        elif i == 0 or tags[i - 1] == 'O':  # conversion IOB1 to IOB2
            tags[i] = 'B' + tag[1:]
        elif tags[i - 1][1:] == tag[1:]:
            continue
        else:  # conversion IOB1 to IOB2
            tags[i] = 'B' + tag[1:]
    return True


def iob_iobes(tags):
    """
    IOB -> IOBES
    """
    new_tags = []
    for i, tag in enumerate(tags):
        if tag == 'O':
            new_tags.append(tag)
        elif tag.split('-')[0] == 'B':
            if i + 1 != len(tags) and \
               tags[i + 1].split('-')[0] == 'I':
                new_tags.append(tag)
            else:
                new_tags.append(tag.replace('B-', 'S-'))
        elif tag.split('-')[0] == 'I':
            if i + 1 < len(tags) and \
                    tags[i + 1].split('-')[0] == 'I':
                new_tags.append(tag)
            else:
                new_tags.append(tag.replace('I-', 'E-'))
        else:
            raise Exception('Invalid IOB format!')
    return new_tags


def iobes_iob(tags):
    """
    IOBES -> IOB
    """
    new_tags = []
    for i, tag in enumerate(tags):
        if tag.split('-')[0] == 'B':
            new_tags.append(tag)
        elif tag.split('-')[0] == 'I':
            new_tags.append(tag)
        elif tag.split('-')[0] == 'S':
            new_tags.append(tag.replace('S-', 'B-'))
        elif tag.split('-')[0] == 'E':
            new_tags.append(tag.replace('E-', 'I-'))
        elif tag.split('-')[0] == 'O':
            new_tags.append(tag)
        else:
            raise Exception('Invalid format!')
    return new_tags


def insert_singletons(words, singletons, p=0.5):
    """
    Replace singletons by the unknown word with a probability p.
    """
    new_words = []
    for word in words:
        if word in singletons and np.random.uniform() < p:
            new_words.append(0)
        else:
            new_words.append(word)
    return new_words


def pad_word_chars(words):
    """
    Pad the characters of the words in a sentence.
    Input:
        - list of lists of ints (list of words, a word being a list of char indexes)
    Output:
        - padded list of lists of ints
        - padded list of lists of ints (where chars are reversed)
        - list of ints corresponding to the index of the last character of each word
    """
    max_length = max([len(word) for word in words])
    char_for = []
    char_rev = []
    char_pos = []
    for word in words:
        padding = [0] * (max_length - len(word))
        char_for.append(word + padding)
        char_rev.append(word[::-1] + padding)
        char_pos.append(len(word) - 1)
    return char_for, char_rev, char_pos


def create_input(data, parameters, add_label, singletons=None):
    """
    Take sentence data and return an input for
    the training or the evaluation function.
    """
    words = data['words']
    chars = data['chars']
    if singletons is not None:
        words = insert_singletons(words, singletons)
    if parameters['cap_dim']:
        caps = data['caps']
    if parameters['pos_dim']:
        pos = data['POSs']
    if parameters['relation']:
        deps = data['deps']
    
    char_for, char_rev, char_pos = pad_word_chars(chars)
    input = []
    if parameters['word_dim']:
        input.append(words)
    if parameters['char_dim']:
        input.append(char_for)
        if parameters['char_bidirect']:
            input.append(char_rev)
        input.append(char_pos)
    if parameters['cap_dim']:
        input.append(caps)
    if parameters['pos_dim']:
        input.append(pos)
    if add_label:
        input.append(data['tags'])
    return input

def create_relation_input(data, parameters, add_label, tag_to_id, relations, relation_to_id, randp=0.2, singletons=None):
    """
    Take sentence data and return an input for
    the training or the evaluation function.
    """
    words = data['words']
    chars = data['chars']
    if singletons is not None:
        words = insert_singletons(words, singletons)
    if parameters['cap_dim']:
        caps = data['caps']
    if parameters['pos_dim']:
        pos = data['POSs']
    if parameters['relation']:
        deps = data['deps']
    
    char_for, char_rev, char_pos = pad_word_chars(chars)
    input = []
    if parameters['word_dim']:
        input.append(words)
    if parameters['char_dim']:
        input.append(char_for)
        if parameters['char_bidirect']:
            input.append(char_rev)
        input.append(char_pos)
    if parameters['cap_dim']:
        input.append(caps)
    if parameters['pos_dim']:
        input.append(pos)
    if parameters['dep_dim']:
        if parameters['isdep']:
            input.append(deps)
    relation_tuples = create_relation_tuples(data, tag_to_id, relations, relation_to_id, randp)
    if not relation_tuples:return None
    entities1, entities2, left_dep_paths_for, left_dep_paths_rev, right_dep_paths_for, right_dep_paths_rev, left_dep_paths_pos, right_dep_paths_pos, relation_labels, distances, entdist, inbetween_path_for, inbetween_path_rev, inbetween_path_pos= create_batch(relation_tuples, data)
    if parameters['isent']:
        input.append(entities1)
        input.append(entities2)
    if parameters['isdep']:
        input.append(left_dep_paths_for)
        input.append(left_dep_paths_rev)
        input.append(right_dep_paths_for)
        input.append(right_dep_paths_rev)
        input.append(left_dep_paths_pos)
        input.append(right_dep_paths_pos)
    if parameters['entdist']:
        input.append(entdist)
    if parameters['distance']:
        input.append(distances)
    if parameters['inbetween_path']:
        input.append(inbetween_path_for)
        input.append(inbetween_path_rev)
        input.append(inbetween_path_pos)
    # if parameters['reverse']:
    #     input.append(reverses)

    if add_label:
        input.append(relation_labels)

    return input


def dfs(key, headlst, path):
    # print sent[key].word
    path.append(headlst[key])
    if headlst[key] == -1:
        return 1
    dfs(headlst[key], headlst, path)
    return 1

def GetDepPath(key1, key2, headlst):
    # index starts with 1, match with xml idx, 0 is root
    left_path = [key1]
    dfs(key1, headlst, left_path)
    left_path = left_path[::-1]
    right_path = [key2]
    dfs(key2, headlst, right_path)
    right_path = right_path[::-1]
    i = 0
    while(i < len(left_path) and i < len(right_path)):
        if left_path[i] != right_path[i]:
            break
        i += 1

    if i>0:
        left_dep_path = left_path[i-1:]
        right_dep_path = right_path[i-1:]
    else:
        pdb.set_trace()
    return left_dep_path, right_dep_path
def create_relation_tuples(data, tag_to_id, relations, relation_to_id, randp):
    O_id = tag_to_id['O']
    entities = [] 
    entity = []
    for i in range(len(data['tags'])):
        if data['tags'][i] == O_id:
            entities.append(entity)
            entity = []
        else:
            entity.append(i)
    entities.append(entity)
    entities = [entity for entity in entities if entity]
    if len(entities) <= 1:return None
    if data['docsent'] in relations:
        relations = relations[data['docsent']]
    else:
        relations = {}
    entity_tuples = []
    for i in range(len(entities)):
        for j in range(i+1, len(entities)):
            phraseidx1 = entities[i][-1]
            phraseidx2 = entities[j][-1]
            inbetween = [idx for idx in range(phraseidx1+1, entities[j][0])]

            if (phraseidx1,phraseidx2) in relations:
                relation  = relations[(phraseidx1,phraseidx2)]
                left_dep_path, right_dep_path = GetDepPath(phraseidx1, phraseidx2, data['headidx'])
                entity_tuple = [entities[i], entities[j], left_dep_path, right_dep_path, relation, abs(j-i)-1, inbetween]

            elif (phraseidx2,phraseidx1) in relations:
                relation  = relations[(phraseidx2,phraseidx1)]
                left_dep_path, right_dep_path = GetDepPath(phraseidx2, phraseidx1, data['headidx'])
                
                entity_tuple = [entities[j], entities[i], left_dep_path, right_dep_path, relation, abs(j-i)-1, inbetween]
                pdb.set_trace()
            else:
                relation  = relation_to_id['None']
                left_dep_path, right_dep_path = GetDepPath(phraseidx1, phraseidx2, data['headidx'])
                
                entity_tuple = [entities[i], entities[j], left_dep_path, right_dep_path, relation, abs(j-i)-1, inbetween]

                # pdb.set_trace()
            if left_dep_path[0] > right_dep_path[0]:pdb.set_trace()
            if entity_tuple[-3] == relation_to_id['None']:
                randnum = random.uniform(0, 1)

                if randnum <= randp:
                    entity_tuples.append(entity_tuple)
            else:
                entity_tuples.append(entity_tuple)    
    return entity_tuples

# def create_batch(entity_tuples, data):
#     entities1 = []
#     entities2 = []
#     left_dep_paths_for = []
#     left_dep_paths_rev = []
#     right_dep_paths_for = []
#     right_dep_paths_rev = []
#     left_dep_path_pos = []
#     right_dep_path_pos = []
#     inbetween_path_for = []
#     inbetween_path_rev = []
#     inbetween_path_pos = []
#     relations = []
#     distances = []
#     entdist = []
#     reverses = []
#     sentl = len(data['tags'])
#     max_left_path = max([len(entity_tuple[2]) for entity_tuple in entity_tuples])
#     max_right_path = max([len(entity_tuple[3]) for entity_tuple in entity_tuples])
#     max_inbetween = max([len(entity_tuple[-2]) for entity_tuple in entity_tuples])
#     for entity_tuple in entity_tuples:
#         entity1 = [0 if i not in entity_tuple[0] else 1 for i in range(sentl)]
#         entity2 = [0 if i not in entity_tuple[1] else 1 for i in range(sentl)]
#         left_path_for = entity_tuple[2] + [0] * (max_left_path - len(entity_tuple[2]))
#         left_path_rev = entity_tuple[2][::-1] + [0] * (max_left_path - len(entity_tuple[2]))
#         left_path_pos = len(entity_tuple[2])-1
#         right_path_for = entity_tuple[3] + [0] * (max_right_path - len(entity_tuple[3])) 
#         right_path_rev = entity_tuple[3][::-1] + [0] * (max_right_path - len(entity_tuple[3]))
#         right_path_pos = len(entity_tuple[3])-1
#         inbetween_for = entity_tuple[-2] + [0] * (max_inbetween - len(entity_tuple[-2]))
#         inbetween_rev = entity_tuple[-2][::-1] + [0] * (max_inbetween - len(entity_tuple[-2]))
#         inbetween_pos = len(entity_tuple[-2])-1
#         entities1.append(entity1)
#         entities2.append(entity2)
#         left_dep_paths_for.append(left_path_for)
#         left_dep_paths_rev.append(left_path_rev)
#         left_dep_path_pos.append(left_path_pos)
#         right_dep_paths_for.append(right_path_for)
#         right_dep_paths_rev.append(right_path_rev)
#         right_dep_path_pos.append(right_path_pos)
#         relations.append(entity_tuple[-4])
#         if entity_tuple[-3] < 4:
#             entdist.append(entity_tuple[-3])
#         else:
#             entdist.append(4)
#         inbetween_path_for.append(inbetween_for)
#         inbetween_path_rev.append(inbetween_rev)
#         inbetween_path_pos.append(inbetween_pos)
#         distance = left_path_pos + right_path_pos
#         if distance < 14:
#             distances.append(distance)
#         else:
#             distances.append(14)
#         reverses.append(entity_tuple[-1])
#     return entities1, entities2, left_dep_paths_for, left_dep_paths_rev, right_dep_paths_for, right_dep_paths_rev, left_dep_path_pos, right_dep_path_pos, relations, distances, entdist, inbetween_path_for, inbetween_path_rev, inbetween_path_pos, reverses
def create_batch(entity_tuples, data):
    entities1 = []
    entities2 = []
    left_dep_paths_for = []
    left_dep_paths_rev = []
    right_dep_paths_for = []
    right_dep_paths_rev = []
    left_dep_path_pos = []
    right_dep_path_pos = []
    inbetween_path_for = []
    inbetween_path_rev = []
    inbetween_path_pos = []
    relations = []
    distances = []
    entdist = []
    sentl = len(data['tags'])
    max_left_path = max([len(entity_tuple[2]) for entity_tuple in entity_tuples])
    max_right_path = max([len(entity_tuple[3]) for entity_tuple in entity_tuples])
    max_inbetween = max([len(entity_tuple[-1]) for entity_tuple in entity_tuples])
    for entity_tuple in entity_tuples:
        entity1 = [0 if i not in entity_tuple[0] else 1 for i in range(sentl)]
        entity2 = [0 if i not in entity_tuple[1] else 1 for i in range(sentl)]
        left_path_for = entity_tuple[2] + [0] * (max_left_path - len(entity_tuple[2]))
        left_path_rev = entity_tuple[2][::-1] + [0] * (max_left_path - len(entity_tuple[2]))
        left_path_pos = len(entity_tuple[2])-1
        right_path_for = entity_tuple[3] + [0] * (max_right_path - len(entity_tuple[3])) 
        right_path_rev = entity_tuple[3][::-1] + [0] * (max_right_path - len(entity_tuple[3]))
        right_path_pos = len(entity_tuple[3])-1
        inbetween_for = entity_tuple[-1] + [0] * (max_inbetween - len(entity_tuple[-1]))
        inbetween_rev = entity_tuple[-1][::-1] + [0] * (max_inbetween - len(entity_tuple[-1]))
        inbetween_pos = len(entity_tuple[-1])-1
        entities1.append(entity1)
        entities2.append(entity2)
        left_dep_paths_for.append(left_path_for)
        left_dep_paths_rev.append(left_path_rev)
        left_dep_path_pos.append(left_path_pos)
        right_dep_paths_for.append(right_path_for)
        right_dep_paths_rev.append(right_path_rev)
        right_dep_path_pos.append(right_path_pos)
        relations.append(entity_tuple[-3])
        if entity_tuple[-2] < 4:
            entdist.append(entity_tuple[-2])
        else:
            entdist.append(4)
        inbetween_path_for.append(inbetween_for)
        inbetween_path_rev.append(inbetween_rev)
        inbetween_path_pos.append(inbetween_pos)
        distance = left_path_pos + right_path_pos
        if distance < 14:
            distances.append(distance)
        else:
            distances.append(14)
    return entities1, entities2, left_dep_paths_for, left_dep_paths_rev, right_dep_paths_for, right_dep_paths_rev, left_dep_path_pos, right_dep_path_pos, relations, distances, entdist, inbetween_path_for, inbetween_path_rev, inbetween_path_pos


def evaluate_relation_macro(parameters, f_eval, raw_sentences, parsed_sentences,
                            id_to_relation, relation_to_id, eval_temp, n_epoch, iftest, relations, tag_to_id, true_path, randp = 0.1):
    """
    Evaluate current model using CoNLL script.
    """

    predictions = []
    r_tag_all = []
    p_tag_all = []
    targets = [id_to_relation[i] for i in range(len(id_to_relation))]
    strings = []
    for raw_sentence, data in zip(raw_sentences, parsed_sentences):
        input = create_relation_input(data, parameters, True, tag_to_id, relations, relation_to_id, randp)
        if not input:continue
        y_reals = input[-1]
        input = input[:-1]
        y_preds = f_eval(*input).argmax(axis=1)
        p_tags = [id_to_relation[y_pred] for y_pred in y_preds]
        p_tag_all += p_tags
        r_tags = [id_to_relation[y_real] for y_real in y_reals]
        r_tag_all += r_tags
        for i in range(len(y_reals)):

            if parameters['reverse'] and parameters['entdist']:
                entity1 = data['phraseID'][input[-7][i][0]]
                entity2 = data['phraseID'][input[-5][i][0]]
                annotation = p_tags[i]
                # entityids.append('\t'.join([data['phraseID'][input[-5][i][0]], data['phraseID'][input[-7][i][0]], p_tags[i], r_tags[i]]))

            elif parameters['reverse'] or parameters['entdist']:
                entity1 = data['phraseID'][input[-6][i][0]]
                entity2 = data['phraseID'][input[-4][i][0]]
                annotation = p_tags[i]
                # entityids.append('\t'.join([data['phraseID'][input[-4][i][0]], data['phraseID'][input[-6][i][0]], p_tags[i], r_tags[i]]))
            else:
                entity1 = data['phraseID'][input[-5][i][0]]
                entity2 = data['phraseID'][input[-3][i][0]]
                annotation = p_tags[i]
                # entityids.append('\t'.join([data['phraseID'][input[-3][i][0]], data['phraseID'][input[-5][i][0]], p_tags[i], r_tags[i]]))
            if 'Reverse' in annotation:
                annotation = annotation.replace('_Reverse','')
                string = annotation + '(' + entity1 + ',' + entity2 + ','+ 'REVERSE)'
            else:
                string = annotation + '(' + entity1 + ',' + entity2  + ')'
            if 'None' in annotation:continue
            strings.append(string)
    if not strings:
        report = classification_report(r_tag_all, p_tag_all, targets)
        return 0


    output_path = os.path.join(eval_temp, "eval.%i.reoutput" % n_epoch)
    scores_path = os.path.join(eval_temp, "eval.%i.scores" % n_epoch)
    output_path = output_path + '.' + iftest
    scores_path = scores_path + '.' + iftest
    write_output(output_path, strings)
    os.system("%s %s %s > %s" % (eval_script, output_path, true_path,  scores_path))
    eval_lines = [l.rstrip() for l in codecs.open(scores_path, 'r', 'utf8')]
    for line in eval_lines:print line
    macro_f1 = eval_lines[-2].split('=')[-1].replace('>>>','').replace(' ','').replace('%','')
    macro_f1 = float(macro_f1)
    return macro_f1
    # report = classification_report(r_tag_all, p_tag_all, targets)
    # print report
    # targets.remove('None')
    # report = classification_report(r_tag_all, p_tag_all, targets)
    # print report
    # D_class_data = report2dict(report)
    
    # return D_class_data['avg / total']['f1-score'], entityids
    
def evaluate_relation_micro(parameters, f_eval, raw_sentences, parsed_sentences,
                            id_to_relation, relation_to_id, eval_temp, n_epoch, iftest, relations, tag_to_id, true_path, randp = 0.1):
    """
    Evaluate current model using CoNLL script.
    """

    predictions = []
    r_tag_all = []
    p_tag_all = []
    targets = [id_to_relation[i] for i in range(len(id_to_relation))]
    strings = []
    for raw_sentence, data in zip(raw_sentences, parsed_sentences):
        input = create_relation_input(data, parameters, True, tag_to_id, relations, relation_to_id, randp)
        if not input:continue
        y_reals = input[-1]
        input = input[:-1]
        y_preds = f_eval(*input).argmax(axis=1)
        p_tags = [id_to_relation[y_pred] for y_pred in y_preds]
        p_tag_all += p_tags
        r_tags = [id_to_relation[y_real] for y_real in y_reals]
        r_tag_all += r_tags
        for i in range(len(y_reals)):

            if parameters['reverse'] and parameters['entdist']:
                entity1 = data['phraseID'][input[-7][i][0]]
                entity2 = data['phraseID'][input[-5][i][0]]
                annotation = p_tags[i]
                # entityids.append('\t'.join([data['phraseID'][input[-5][i][0]], data['phraseID'][input[-7][i][0]], p_tags[i], r_tags[i]]))

            elif parameters['reverse'] or parameters['entdist']:
                entity1 = data['phraseID'][input[-6][i][0]]
                entity2 = data['phraseID'][input[-4][i][0]]
                annotation = p_tags[i]
                # entityids.append('\t'.join([data['phraseID'][input[-4][i][0]], data['phraseID'][input[-6][i][0]], p_tags[i], r_tags[i]]))
            else:
                entity1 = data['phraseID'][input[-5][i][0]]
                entity2 = data['phraseID'][input[-3][i][0]]
                annotation = p_tags[i]
                # entityids.append('\t'.join([data['phraseID'][input[-3][i][0]], data['phraseID'][input[-5][i][0]], p_tags[i], r_tags[i]]))
            if 'Reverse' in annotation:
                annotation = annotation.replace('_Reverse','')
                string = annotation + '(' + entity1 + ',' + entity2 + ','+ 'REVERSE)'
            else:
                string = annotation + '(' + entity1 + ',' + entity2  + ')'
            if 'None' in annotation:continue
            strings.append(string)
    if not strings:
        report = classification_report(r_tag_all, p_tag_all, targets)
        return 0
    output_path = os.path.join(eval_temp, "eval.%i.reoutput" % n_epoch)
    scores_path = os.path.join(eval_temp, "eval.%i.scores" % n_epoch)
    output_path = output_path + '.' + iftest
    scores_path = scores_path + '.' + iftest
    write_output(output_path, strings)
    os.system("%s %s %s > %s" % (eval_script, output_path, true_path,  scores_path))
    eval_lines = [l.rstrip() for l in codecs.open(scores_path, 'r', 'utf8')]

    for line in eval_lines:print line
    micro_f1 = eval_lines[-9].split('=')[-1].replace(' ','').replace('%','')
    micro_f1 = float(micro_f1)
    return micro_f1
    # report = classification_report(r_tag_all, p_tag_all, targets)
    # print report
    # targets.remove('None')
    # report = classification_report(r_tag_all, p_tag_all, targets)
    # print report
    # D_class_data = report2dict(report)
    
    # return D_class_data['avg / total']['f1-score'], entityids

def evaluate_relation(parameters, f_eval, raw_sentences, parsed_sentences,
                      id_to_relation, relation_to_id, eval_temp, n_epoch, iftest, relations, tag_to_id, randp = 0.1):
    """
    Evaluate current model using CoNLL script.
    """

    predictions = []
    r_tag_all = []
    p_tag_all = []
    targets = [id_to_relation[i] for i in range(len(id_to_relation))]
    entityids = []
    for raw_sentence, data in zip(raw_sentences, parsed_sentences):
        input = create_relation_input(data, parameters, True, tag_to_id, relations, relation_to_id, randp)
        if not input:continue
        y_reals = input[-1]
        input = input[:-1]
        y_preds = f_eval(*input).argmax(axis=1)
        p_tags = [id_to_relation[y_pred] for y_pred in y_preds]
        p_tag_all += p_tags
        r_tags = [id_to_relation[y_real] for y_real in y_reals]
        r_tag_all += r_tags
        for i in range(len(y_reals)):

            if parameters['distance'] and parameters['entdist']:
                entityids.append('\t'.join([data['phraseID'][input[-5][i][0]], data['phraseID'][input[-7][i][0]], p_tags[i], r_tags[i]]))

            elif parameters['distance'] or parameters['entdist']:
                entityids.append('\t'.join([data['phraseID'][input[-4][i][0]], data['phraseID'][input[-6][i][0]], p_tags[i], r_tags[i]]))
            else:
                entityids.append('\t'.join([data['phraseID'][input[-3][i][0]], data['phraseID'][input[-5][i][0]], p_tags[i], r_tags[i]]))
                
                
    report = classification_report(r_tag_all, p_tag_all, targets)
    print report
    
def print_test_score(parameters, f_eval, raw_sentences, parsed_sentences,
                      id_to_relation, relation_to_id, eval_temp, n_epoch, iftest, relations, tag_to_id, randp = 1):
    """
    Evaluate current model using CoNLL script.
    """

    predictions = []
    r_tag_all = []
    p_tag_all = []
    targets = [id_to_relation[i] for i in range(len(id_to_relation))]
    entityids = []
    entityids.append(' '.join(targets))
    for raw_sentence, data in zip(raw_sentences, parsed_sentences):
        input = create_relation_input(data, parameters, True, tag_to_id, relations, relation_to_id, randp)
        if not input:continue
        y_reals = input[-1]
        input = input[:-1]
        y_preds = f_eval(*input)
        # p_tags = [id_to_relation[y_pred] for y_pred in y_preds]
        # p_tag_all += p_tags

        for i in range(len(y_reals)):

            if parameters['distance'] and parameters['entdist']:
                p_tags = [str(pred) for pred in y_preds[i,:]]
                entityids.append('\t'.join([data['phraseID'][input[-5][i][0]], data['phraseID'][input[-7][i][0]], ' '.join(p_tags)]))

            elif parameters['distance'] or parameters['entdist']:
                p_tags = [str(pred) for pred in y_preds[i,:]]
                entityids.append('\t'.join([data['phraseID'][input[-4][i][0]], data['phraseID'][input[-6][i][0]], ' '.join(p_tags)]))
            else:
                p_tags = [str(pred) for pred in y_preds[i,:]]
                entityids.append('\t'.join([data['phraseID'][input[-3][i][0]], data['phraseID'][input[-5][i][0]], ' '.join(p_tags)]))
    output_path = os.path.join(eval_temp, "eval.%i.reoutput" % n_epoch)
    output_path = output_path + '.' + iftest
    write_output(output_path,entityids)
                

def write_output(output_path,entityids):

    with codecs.open(output_path, 'w', 'utf8') as f:
        f.write("\n".join(entityids))
    
def evaluate(parameters, f_eval, raw_sentences, parsed_sentences,
             id_to_tag, eval_temp, n_epoch, iftest):
    """
    Evaluate current model using CoNLL script.
    """
    n_tags = len(id_to_tag)
    predictions = []
    count = np.zeros((n_tags, n_tags), dtype=np.int32)
    r_tag_all = []
    p_tag_all = []
    for raw_sentence, data in zip(raw_sentences, parsed_sentences):
        input = create_input(data, parameters, False)
        if parameters['crf']:
            y_preds = np.array(f_eval(*input)[0])[1:-1]
        else:
            y_preds = f_eval(*input).argmax(axis=1)
        y_reals = np.array(data['tags']).astype(np.int32)
        assert len(y_preds) == len(y_reals)
        p_tags = [id_to_tag[y_pred] for y_pred in y_preds]
        r_tags = [id_to_tag[y_real] for y_real in y_reals]
        if parameters['tag_scheme'] == 'iobes':
            p_tags = iobes_iob(p_tags)
            r_tags = iobes_iob(r_tags)
        r_tag_all += [tag.split('-')[1] if '-' in tag else tag for tag in r_tags ]
        p_tag_all += [tag.split('-')[1] if '-' in tag else tag for tag in p_tags ]
        for i, (y_pred, y_real) in enumerate(zip(y_preds, y_reals)):
            new_line = " ".join(raw_sentence[i] + [r_tags[i], p_tags[i]])
            predictions.append(new_line)
            count[y_real, y_pred] += 1
        predictions.append("")
    # Write predictions to disk and run CoNLL script externally
    eval_id = n_epoch
    print 'The ' + str(n_epoch) + 'th validation.'
    output_path = os.path.join(eval_temp, "eval.%i.output" % eval_id)
    scores_path = os.path.join(eval_temp, "eval.%i.scores" % eval_id)
    output_path = output_path + '.' + iftest
    scores_path = scores_path + '.' + iftest
    print output_path
    with codecs.open(output_path, 'w', 'utf8') as f:
        f.write("\n".join(predictions))
    os.system("%s < %s > %s" % (eval_script, output_path, scores_path))
    targets = ['O','Task','Process','Material']
    report = classification_report(r_tag_all, p_tag_all, targets)
    with open(scores_path, 'a') as f:
        f.write('Catergory results\n')
        f.write(report + '\n')
        
    targets = ['O','Keyword']
    r_tag_all = ['O' if tag=='O' else 'Keyword' for tag in r_tag_all]
    p_tag_all = ['O' if tag=='O' else 'Keyword' for tag in p_tag_all]
    report = classification_report(r_tag_all, p_tag_all, targets)
    with open(scores_path, 'a') as f:
        f.write('Keyword results\n')
        f.write(report + '\n')


    # CoNLL evaluation results
    eval_lines = [l.rstrip() for l in codecs.open(scores_path, 'r', 'utf8')]
    for line in eval_lines:
        print line

    # Remove temp files
    # os.remove(output_path)
    # os.remove(scores_path)

    # Confusion matrix with accuracy for each tag
    print ("{: >2}{: >7}{: >7}%s{: >9}" % ("{: >7}" * n_tags)).format(
        "ID", "NE", "Total",
        *([id_to_tag[i] for i in xrange(n_tags)] + ["Percent"])
    )
    for i in xrange(n_tags):
        print ("{: >2}{: >7}{: >7}%s{: >9}" % ("{: >7}" * n_tags)).format(
            str(i), id_to_tag[i], str(count[i].sum()),
            *([count[i][j] for j in xrange(n_tags)] +
              ["%.3f" % (count[i][i] * 100. / max(1, count[i].sum()))])
        )

    # Global accuracy
    print "%i/%i (%.5f%%)" % (
        count.trace(), count.sum(), 100. * count.trace() / max(1, count.sum())
    )

    # F1 on all entities
    return float(eval_lines[1].strip().split()[-1])

def test(parameters, f_eval, raw_sentences, parsed_sentences,
             id_to_tag, dictionary_tags, eval_temp, n_epoch, fn, f_eval_softmax):
    """
    Evaluate current model using CoNLL script.
    """
    n_tags = len(id_to_tag)
    predictions = []
    count = np.zeros((n_tags, n_tags), dtype=np.int32)
    eval_id = n_epoch
    output_path = fn
    print fn
    with codecs.open(output_path, 'w', 'utf8') as f:
        for raw_sentence, data in zip(raw_sentences, parsed_sentences):
            input = create_input(data, parameters, False)
            try:
                if parameters['crf']:
                    preds = f_eval(*input)
                    scores = preds[1][range(len(input[0])+2),preds[0]][1:-1]
                    y_preds = np.array(preds[0])[1:-1]

                else:
                    y_preds = f_eval(*input).argmax(axis=1)
            except:
                continue
            p_tags = [id_to_tag[y_pred] for y_pred in y_preds]
            if parameters['tag_scheme'] == 'iobes':
                p_tags = iobes_iob(p_tags)
            for i, y_pred in enumerate(y_preds):
                if parameters['crf']:
                    new_line = " ".join([raw_sentence[i][0]] + [p_tags[i]] + raw_sentence[i][2:] + [str(scores[i])])
                else:
                    new_line = " ".join([raw_sentence[i][0]] + [p_tags[i]] + raw_sentence[i][2:])
                f.write(new_line)
                f.write('\n')
            f.write('\n')

    # Write predictions to disk and run CoNLL script externally
    # print 'The ' + str(n_epoch) + 'th validation.'

