import os
import re
import numpy as np
import scipy.io
import theano
import theano.tensor as T
import codecs
import cPickle
import pdb
from utils import shared, set_values, get_name
from nn import HiddenLayer, EmbeddingLayer, DropoutLayer, LSTM, forward
from optimization import Optimization


class Model(object):
    """
    Network architecture.
    """
    def __init__(self, parameters=None, models_path=None, load_path=None):
        """
        Initialize the model. We either provide the parameters and a path where
        we store the models, or the location of a trained model.
        """
        if load_path is None:
            assert parameters and models_path
            # Create a name based on the parameters
            self.parameters = parameters
            self.name = get_name(parameters)
            # Model location
            model_path = os.path.join(models_path, self.name)
            self.model_path = model_path
            self.parameters_path = os.path.join(model_path, 'parameters.pkl')
            self.mappings_path = os.path.join(model_path, 'mappings.pkl')
            # Create directory for the model if it does not exist
            if not os.path.exists(self.model_path):
                os.makedirs(self.model_path)
            # Save the parameters to disk
            with open(self.parameters_path, 'wb') as f:
                cPickle.dump(parameters, f)
        else:
            # Model location
            # Model location
            if parameters:
                self.name = get_name(parameters)
                model_path = os.path.join(models_path, self.name)
                self.model_path = model_path
                if not os.path.exists(self.model_path):
                    os.makedirs(self.model_path)
                self.parameters_path = os.path.join(model_path, 'parameters.pkl')
                self.mappings_path = os.path.join(model_path, 'mappings.pkl')
                with open(self.parameters_path, 'wb') as f:
                    cPickle.dump(parameters, f)
            self.load_path = load_path
            self.parameters_path_load = os.path.join(load_path, 'parameters.pkl')
            self.mappings_path_load = os.path.join(load_path, 'mappings.pkl')
            # Load the parameters and the mappings from disk
            # with open(self.parameters_path_load, 'rb') as f:
            #     self.parameters = cPickle.load(f)
            self.parameters = parameters
            self.reload_mappings()
        self.components = {}

    def save_mappings(self, id_to_word, id_to_char, id_to_tag, id_to_POS, id_to_dep=None, id_to_relation=None):
        """
        We need to save the mappings if we want to use the model later.
        """
        self.id_to_word = id_to_word
        self.id_to_char = id_to_char
        self.id_to_tag = id_to_tag
        if self.parameters['pos_dim']:
            self.id_to_POS = id_to_POS
        if id_to_relation!=None:
            self.id_to_relation = id_to_relation
            self.id_to_dep = id_to_dep
            
        with open(self.mappings_path, 'wb') as f:
            print self.mappings_path
            mappings = {
                'id_to_word': self.id_to_word,
                'id_to_char': self.id_to_char,
                'id_to_tag': self.id_to_tag,
            }
            if self.parameters['pos_dim']:
                mappings['id_to_POS'] = self.id_to_POS
            if id_to_relation!=None:
                mappings['id_to_relation'] = self.id_to_relation
                mappings['id_to_dep'] = self.id_to_dep
            cPickle.dump(mappings, f)

    def reload_mappings(self):
        """
        Load mappings from disk.
        """
        with open(self.mappings_path_load, 'rb') as f:
            mappings = cPickle.load(f)
        self.id_to_word = mappings['id_to_word']
        self.id_to_char = mappings['id_to_char']
        self.id_to_tag = mappings['id_to_tag']
        if self.parameters['pos_dim']:
            self.id_to_POS = mappings['id_to_POS']

        if 'id_to_dep' in mappings:
            self.id_to_dep = mappings['id_to_dep']
        if 'id_to_relation' in mappings:
            self.id_to_relation = mappings['id_to_relation']
    def add_component(self, param):
        """
        Add a new parameter to the network.
        """
        if param.name in self.components:
            raise Exception('The network already has a parameter "%s"!'
                            % param.name)
        self.components[param.name] = param
        print param.name
    def save(self):
        """
        Write components values to disk.
        """
        for name, param in self.components.items():
            param_path = os.path.join(self.model_path, "%s.mat" % name)
            if hasattr(param, 'params'):
                param_values = {p.name: p.get_value() for p in param.params}
            else:
                param_values = {name: param.get_value()}
            scipy.io.savemat(param_path, param_values)

    def reload(self):
        """
        Load components values from disk.
        """
        print 'loading from ' + self.load_path
        npara = 0
        for name, param in self.components.items():
            # if 'final_layer' in name or 'trans' in name or 'word_lstm' in name:
            #     continue
            param_path = os.path.join(self.load_path, "%s.mat" % name)
            param_values = scipy.io.loadmat(param_path)
            if hasattr(param, 'params'):
                for p in param.params:
                    set_values(p.name, p, param_values[p.name])
                    npara += 1
            else:
                set_values(name, param, param_values[name])
                npara += 1
        print 'number of loaded params'
        print npara
    def reload_semeval2010(self):
        """
        Load components values from disk.
        """
        print 'loading from ' + self.load_path
        npara = 0
        for name, param in self.components.items():
            # if 'final_layer' in name or 'trans' in name or 'word_lstm' in name:
            #     continue
            if 'relation_softmax' in name:continue
            if 'relation_tanh' in name:continue
            param_path = os.path.join(self.load_path, "%s.mat" % name)
            if not os.path.exists(param_path):
                print param_path + ' not exists'
                continue
            param_values = scipy.io.loadmat(param_path)
            if hasattr(param, 'params'):
                for p in param.params:
                    set_values(p.name, p, param_values[p.name])
                    npara += 1
            else:
                set_values(name, param, param_values[name])
                npara += 1
        print 'number of loaded params'
        print npara

        # fid = open('/homes/luanyi/pubanal/project/code/char_lstm/models/Adam_tie_lstm_25_25_2016111114_25689/model4.pkl.seq2seq')
        # x = cPickle.load(fid)
        # fid.close()
        # for param in self.components['char_lstm_for'].params:
        #     name = param.name
        #     if name in x:
        #         param.set_value(x[name].astype(np.float32))
                                
        # self.components['char_layer'].params[0].set_value(x['Emb'].astype(np.float32))

        
        # fid = open('/homes/luanyi/pubanal/project/code/char_lstm/models/SGD_tie_lstm_25_25_2016111110_14674/model29.pkl.seq2seq')
        # x = cPickle.load(fid)
        # fid.close()
        # for param in self.components['char_lstm_rev'].params:
        #     name = param.name
        #     if name in x:
        #         param.set_value(x[name].astype(np.float32))
        
    def build(self,
              dropout,
              char_dim,
              char_lstm_dim,
              char_bidirect,
              word_dim,
              word_lstm_dim,
              word_bidirect,
              lr_method,
              pos_dim,
              dep_dim,
              pre_emb,
              pre_emb_dep,
              crf,
              cap_dim,
              relation,
              training=True,
              **kwargs
              ):
        """
        Build the network.
        """
        # Training parameters
        n_words = len(self.id_to_word)
        n_chars = len(self.id_to_char)
        n_tags = len(self.id_to_tag)
        if relation:
            n_relation = len(self.id_to_relation)
            relation_inputs = []

        if dep_dim:
            n_dep = len(self.id_to_dep)
        # Number of capitalization features
        if cap_dim:
            n_cap = 4
        if pos_dim:
            n_POS = len(self.id_to_POS)
            
        is_entdist = self.parameters['entdist']
        is_distance = self.parameters['distance']
        # is_reverse = self.parameters['reverse']
        is_inbetween_path = self.parameters['inbetween_path']
        isdep = self.parameters['isdep']
        isent = self.parameters['isent']

                    
        # Network variables
        is_train = T.iscalar('is_train')
        word_ids = T.ivector(name='word_ids')
        if relation:
            if isent:
                entity1_mask = T.fmatrix(name='entity1_mask')
                entity2_mask = T.fmatrix(name='entity2_mask')
            if isdep:
                path_left_for_ids = T.imatrix(name='path_left_for_ids') 
                path_left_rev_ids = T.imatrix(name='path_left_rev_ids')
                path_right_for_ids = T.imatrix(name='path_right_for_ids')
                path_right_rev_ids = T.imatrix(name='path_right_rev_ids')
                path_left_pos = T.ivector(name='path_left_pos')
                path_right_pos = T.ivector(name='path_right_pos')
            if is_entdist:
                entdist = T.ivector(name='entity_dist')
            # if is_reverse:
            #     reverse = T.ivector(name='reverse')
            if is_distance:
                distance = T.ivector(name='distance')
            if is_inbetween_path: 
                inbetween_path_for = T.ivector(name='inbetween_path_for')
                inbetween_path_rev = T.ivector(name='inbetween_path_rev')
                inbetween_path_pos = T.ivector(name='inbetween_path_pos')
                
            relation_ids = T.ivector(name='relation_ids')
            
        char_for_ids = T.imatrix(name='char_for_ids')
        char_rev_ids = T.imatrix(name='char_rev_ids')
        char_pos_ids = T.ivector(name='char_pos_ids')
        probs = T.ivector(name='probs')
        tag_ids = T.ivector(name='tag_ids')
        if cap_dim:
            cap_ids = T.ivector(name='cap_ids')
        if pos_dim:
            pos_ids = T.ivector(name='pos_ids')
        if dep_dim:
            dep_ids = T.ivector(name='dep_ids')

                        
        # Sentence length
        s_len = (word_ids if word_dim else char_pos_ids).shape[0]

        # Final input (all word features)
        input_dim = 0
        inputs = []

        #
        # Word inputs
        #
        if word_dim:
            input_dim += word_dim
            word_layer = EmbeddingLayer(n_words, word_dim, name='word_layer')
            word_input = word_layer.link(word_ids)
            inputs.append(word_input)
            # Initialize with pretrained embeddings
            if pre_emb and training:
                new_weights = word_layer.embeddings.get_value()
                print 'Loading pretrained embeddings from %s...' % pre_emb
                pretrained = {}
                emb_invalid = 0
                for i, line in enumerate(codecs.open(pre_emb, 'r', 'utf-8')):
                    line = line.rstrip().split()
                    if len(line) == word_dim + 1:
                        pretrained[line[0]] = np.array(
                            [float(x) for x in line[1:]]
                        ).astype(np.float32)
                    else:
                        emb_invalid += 1
                if emb_invalid > 0:
                    print 'WARNING: %i invalid lines' % emb_invalid
                c_found = 0
                c_lower = 0
                c_zeros = 0
                # Lookup table initialization
                for i in xrange(n_words):
                    word = self.id_to_word[i]
                    if word in pretrained:
                        new_weights[i] = pretrained[word]
                        c_found += 1
                    elif word.lower() in pretrained:
                        new_weights[i] = pretrained[word.lower()]
                        c_lower += 1
                    elif re.sub('\d', '0', word.lower()) in pretrained:
                        new_weights[i] = pretrained[
                            re.sub('\d', '0', word.lower())
                        ]
                        c_zeros += 1
                word_layer.embeddings.set_value(new_weights)
                print 'Loaded %i pretrained embeddings.' % len(pretrained)
                print ('%i / %i (%.4f%%) words have been initialized with '
                       'pretrained embeddings.') % (
                            c_found + c_lower + c_zeros, n_words,
                            100. * (c_found + c_lower + c_zeros) / n_words
                      )
                print ('%i found directly, %i after lowercasing, '
                       '%i after lowercasing + zero.') % (
                          c_found, c_lower, c_zeros
                      )

        #
        # Chars inputs
        #
        if char_dim:
            input_dim += char_lstm_dim
            char_layer = EmbeddingLayer(n_chars, char_dim, name='char_layer')

            char_lstm_for = LSTM(char_dim, char_lstm_dim, with_batch=True,
                                 name='char_lstm_for')
            char_lstm_rev = LSTM(char_dim, char_lstm_dim, with_batch=True,
                                 name='char_lstm_rev')

            char_lstm_for.link(char_layer.link(char_for_ids))
            char_lstm_rev.link(char_layer.link(char_rev_ids))
            #len(T.arange(s_len)) == len(char_pos_ids)
            #char_for_output shape = batch_size*lstmh
            char_for_output = char_lstm_for.h.dimshuffle((1, 0, 2))[
                T.arange(s_len), char_pos_ids
            ]
            char_rev_output = char_lstm_rev.h.dimshuffle((1, 0, 2))[
                T.arange(s_len), char_pos_ids
            ]

            inputs.append(char_for_output)
            if char_bidirect:
                inputs.append(char_rev_output)
                input_dim += char_lstm_dim

        #
        # Capitalization feature
        #
        if cap_dim:
            input_dim += cap_dim
            cap_layer = EmbeddingLayer(n_cap, cap_dim, name='cap_layer')
            inputs.append(cap_layer.link(cap_ids))
        if pos_dim:
            input_dim += pos_dim
            pos_layer = EmbeddingLayer(n_POS, pos_dim, name='pos_layer')
            inputs.append(pos_layer.link(pos_ids))



        # Prepare final input
        if len(inputs) != 1:
            inputs = T.concatenate(inputs, axis=1)

        #
        # Dropout on final input
        #
        if dropout:
            dropout_layer = DropoutLayer(p=dropout)
            input_train = dropout_layer.link(inputs)
            input_test = (1 - dropout) * inputs
            inputs = T.switch(T.neq(is_train, 0), input_train, input_test)

        # LSTM for words
        word_lstm_for = LSTM(input_dim, word_lstm_dim, with_batch=False,
                             name='word_lstm_for')
        word_lstm_rev = LSTM(input_dim, word_lstm_dim, with_batch=False,
                             name='word_lstm_rev')
        word_lstm_for.link(inputs)
        word_lstm_rev.link(inputs[::-1, :])
        word_for_output = word_lstm_for.h
        word_rev_output = word_lstm_rev.h[::-1, :]
        if word_bidirect:
            seq_output = T.concatenate(
                [word_for_output, word_rev_output],
                axis=1
            )
            tanh_layer = HiddenLayer(2 * word_lstm_dim, word_lstm_dim,
                                     name='tanh_layer', activation='tanh')
            final_output = tanh_layer.link(seq_output)
        else:
            seq_output = final_output = word_for_output

        
        # Sentence to Named Entity tags - Score
        final_layer = HiddenLayer(word_lstm_dim, n_tags, name='final_layer',
                                  activation=(None if crf else 'softmax'))
        tags_scores = final_layer.link(final_output)
        tags_scores_softmax = tags_scores

        # No CRF
        if not crf:
            cost = T.nnet.categorical_crossentropy(tags_scores, tag_ids).mean()
        # CRF
        else:
            transitions = shared((n_tags + 2, n_tags + 2), 'transitions')

            small = -1000
            b_s = np.array([[small] * n_tags + [0, small]]).astype(np.float32)
            e_s = np.array([[small] * n_tags + [small, 0]]).astype(np.float32)
            observations = T.concatenate(
                [tags_scores, small * T.ones((s_len, 2))],
                axis=1
            )
            observations = T.concatenate(
                [b_s, observations, e_s],
                axis=0
            )

            # Score from tags
            real_path_score = tags_scores[T.arange(s_len), tag_ids].sum()

            # Score from transitions
            b_id = theano.shared(value=np.array([n_tags], dtype=np.int32))
            e_id = theano.shared(value=np.array([n_tags + 1], dtype=np.int32))
            padded_tags_ids = T.concatenate([b_id, tag_ids, e_id], axis=0)
            real_path_score += transitions[
                padded_tags_ids[T.arange(s_len + 1)],
                padded_tags_ids[T.arange(s_len + 1) + 1]
            ].sum()

            all_paths_scores = forward(observations, transitions)
            cost = - (real_path_score - all_paths_scores)

        if relation:
            if dep_dim:
                        
                dep_layer = EmbeddingLayer(n_dep, dep_dim, name='dep_layer')
                dep_input = dep_layer.link(dep_ids)

            nrelation = path_left_pos.shape[0]
            relation_inputs = []
            relation_dim = 0
            #entity1 representation (seq_output size = sequence_length* lstmh_dim)
            #entity1_mask size = batch_size* sequence_length
            if is_entdist:
                entdist_layer = EmbeddingLayer(5, 25, name='entdist_layer')
                relation_inputs.append(entdist_layer.link(entdist))
                relation_dim += 25
            # if is_reverse:
            #     reverse_layer = EmbeddingLayer(2, 4, name='reverse')
            #     relation_inputs.append(reverse_layer.link(reverse))
            #     relation_dim += 4
            if is_distance:
                
                distance_layer = EmbeddingLayer(15, 25, name='distance_layer')
                relation_inputs.append(distance_layer.link(distance))
                relation_dim += 25
            if isent:
                entity_inputs = []
                seq_output1 = T.alloc(seq_output, entity1_mask.shape[0], seq_output.shape[0], seq_output.shape[1])
                entity1_seq = seq_output1.dimshuffle(2,0,1)*entity1_mask
                #entity1_seq size = lstmh_dim * batch_size* sequence_length
                entity1_seq = T.sum(entity1_seq, axis=2).dimshuffle(1,0)

                entity_inputs.append(entity1_seq)
                #entity1_seq size = batch_size*lstmh(200)
                seq_output2 = T.alloc(seq_output, entity2_mask.shape[0], seq_output.shape[0], seq_output.shape[1])
                entity2_seq = seq_output2.dimshuffle(2,0,1)*entity2_mask
                entity2_seq = T.sum(entity2_seq, axis=2).dimshuffle(1,0)

                entity_inputs.append(entity2_seq)
                entity_inputs = T.concatenate(entity_inputs, axis=1)
                entity_tanh_layer = HiddenLayer(4 * word_lstm_dim, word_lstm_dim, name='entity_tanh_layer', activation='tanh')
                entity_output = entity_tanh_layer.link(entity_inputs)
                relation_inputs.append(entity_output)
                relation_dim+= word_lstm_dim
            #left path
            if isdep:
                relation_for_inputs = []
                relation_rev_inputs = []
                relation_input_dim = 0
                relation_for_inputs.append(seq_output[path_left_for_ids])
                relation_input_dim += 2 * word_lstm_dim
                if dep_dim:
                    relation_for_inputs.append(dep_input[path_left_for_ids])
                    relation_input_dim += dep_dim
                relation_rev_inputs.append(seq_output[path_left_rev_ids])
                relation_rev_inputs.append(dep_input[path_left_rev_ids])
                #input size=batchsize* seqlen * outdim
                relation_for_inputs = T.concatenate(relation_for_inputs, axis=2)
                relation_rev_inputs = T.concatenate(relation_rev_inputs, axis=2)
                path_left_lstm_for = LSTM(relation_input_dim, word_lstm_dim, with_batch=True, name='path_left_lstm_for')
                path_left_lstm_rev = LSTM(relation_input_dim, word_lstm_dim, with_batch=True, name='path_left_lstm_rev')
                path_left_lstm_for.link(relation_for_inputs)
                path_left_lstm_rev.link(relation_rev_inputs)
                #obtain last state
                left_for_output = path_left_lstm_for.h.dimshuffle((1, 0, 2))[
                    T.arange(nrelation), path_left_pos
                ]
                #left_for_output shape = nrelation*lstmh
                left_rev_output = path_left_lstm_rev.h.dimshuffle((1, 0, 2))[
                    T.arange(nrelation), path_left_pos
                ]
                relation_inputs.append(left_for_output)
                relation_inputs.append(left_rev_output)
            
                #right path
                relation_for_inputs = []
                relation_rev_inputs = []
                relation_input_dim = 0
                relation_for_inputs.append(seq_output[path_right_for_ids])
                relation_input_dim += 2 * word_lstm_dim
                if dep_dim:
                    relation_for_inputs.append(dep_input[path_right_for_ids])
                    relation_input_dim += dep_dim
                relation_rev_inputs.append(seq_output[path_right_rev_ids])
                if dep_dim:
                    relation_rev_inputs.append(dep_input[path_right_rev_ids])
                relation_for_inputs = T.concatenate(relation_for_inputs, axis=2)
                relation_rev_inputs = T.concatenate(relation_rev_inputs, axis=2)
                path_right_lstm_for = LSTM(relation_input_dim, word_lstm_dim, with_batch=True, name='path_right_lstm_for')
                path_right_lstm_rev = LSTM(relation_input_dim, word_lstm_dim, with_batch=True, name='path_right_lstm_rev')
                path_right_lstm_for.link(relation_for_inputs)
                path_right_lstm_rev.link(relation_rev_inputs)
                #obtain last state
                right_for_output = path_right_lstm_for.h.dimshuffle((1, 0, 2))[
                    T.arange(nrelation), path_right_pos
                ]
                right_rev_output = path_right_lstm_rev.h.dimshuffle((1, 0, 2))[
                    T.arange(nrelation), path_right_pos
                ]
                relation_inputs.append(right_for_output)
                relation_inputs.append(right_rev_output)
                relation_dim += 400
            #predict relation
            relation_inputs = T.concatenate(relation_inputs, axis=1)
            print 'relation dim:'
            print relation_dim
            relation_tanh_layer = HiddenLayer(relation_dim, word_lstm_dim, name='relation_tanh_layer', activation='tanh')
            
            # if isdep:
            #     if isent:
            #         relation_tanh_layer = HiddenLayer(5 * word_lstm_dim, word_lstm_dim, name='relation_tanh_layer', activation='tanh')
            #     else:
            #         relation_tanh_layer = HiddenLayer(4 * word_lstm_dim, word_lstm_dim, name='relation_tanh_layer', activation='tanh')
                    
            # else:
            #     relation_tanh_layer = HiddenLayer(4 * word_lstm_dim, word_lstm_dim, name='relation_tanh_layer', activation='tanh')
            relation_output = relation_tanh_layer.link(relation_inputs)
            relation_softmax = HiddenLayer(word_lstm_dim, n_relation, name='relation_softmax', activation='softmax')
            relation_scores = relation_softmax.link(relation_output)
            relation_cost = T.nnet.categorical_crossentropy(relation_scores, relation_ids).mean()

            
        # Network parameters
        params = []
        if word_dim:
            self.add_component(word_layer)
            params.extend(word_layer.params)
        if char_dim:
            self.add_component(char_layer)
            self.add_component(char_lstm_for)
            params.extend(char_layer.params)
            params.extend(char_lstm_for.params)
            if char_bidirect:
                self.add_component(char_lstm_rev)
                params.extend(char_lstm_rev.params)
        self.add_component(word_lstm_for)
        params.extend(word_lstm_for.params)
        if word_bidirect:
            print 'BUUUUUGGGGGGG component'
            self.add_component(word_lstm_rev)
            params.extend(word_lstm_rev.params)
        if cap_dim:
            self.add_component(cap_layer)
            params.extend(cap_layer.params)
        if pos_dim:
            self.add_component(pos_layer)
            params.extend(pos_layer.params)
        if dep_dim:
            self.add_component(dep_layer)
            params.extend(dep_layer.params)
        if relation:
            if isdep:
                if isent:
                    self.add_component(entity_tanh_layer)
                    params.extend(entity_tanh_layer.params)
                self.add_component(path_left_lstm_for)
                params.extend(path_left_lstm_for.params)
                self.add_component(path_left_lstm_rev)
                params.extend(path_left_lstm_rev.params)
                self.add_component(path_right_lstm_for)
                params.extend(path_right_lstm_for.params)
                self.add_component(path_right_lstm_rev)
                params.extend(path_right_lstm_rev.params)
            if is_entdist:
                self.add_component(entdist_layer)
                params.extend(entdist_layer.params)
            # if is_reverse:
            #     self.add_component(reverse_layer)
            #     params.extend(reverse_layer.params)
            if is_distance:
                self.add_component(distance_layer)
                params.extend(distance_layer.params)
            self.add_component(relation_tanh_layer)
            params.extend(relation_tanh_layer.params)
            self.add_component(relation_softmax)
            params.extend(relation_softmax.params)
        else:
            self.add_component(final_layer)
            params.extend(final_layer.params)
            if crf:
                self.add_component(transitions)
                params.append(transitions)
            if word_bidirect:
                self.add_component(tanh_layer)
                params.extend(tanh_layer.params)
        print 'number of params'
        print len(params)
        # Prepare train and eval inputs
        eval_inputs = []
        if word_dim:
            eval_inputs.append(word_ids)
        if char_dim:
            eval_inputs.append(char_for_ids)
            if char_bidirect:
                eval_inputs.append(char_rev_ids)
            eval_inputs.append(char_pos_ids)
        if cap_dim:
            eval_inputs.append(cap_ids)
        if pos_dim:
            eval_inputs.append(pos_ids)

        if isdep:
            if isent:
                relation_eval_inputs = eval_inputs + [dep_ids, entity1_mask, entity2_mask, path_left_for_ids, path_left_rev_ids, path_right_for_ids, path_right_rev_ids, path_left_pos, path_right_pos]# todo add entity labels to input
            else:
                relation_eval_inputs = eval_inputs + [dep_ids, path_left_for_ids, path_left_rev_ids, path_right_for_ids, path_right_rev_ids, path_left_pos, path_right_pos]# todo add entity labels to input
            
        else:
            relation_eval_inputs = eval_inputs + [entity1_mask, entity2_mask]# todo add entity labels to input
        if is_entdist:
            relation_eval_inputs.append(entdist)
        if is_distance:
            relation_eval_inputs.append(distance)
        # if is_reverse:
        #     relation_eval_inputs.append(reverse)
        relation_train_inputs = relation_eval_inputs + [relation_ids]
        train_inputs = eval_inputs + [tag_ids]

        # Parse optimization method parameters
        if "-" in lr_method:
            lr_method_name = lr_method[:lr_method.find('-')]
            lr_method_parameters = {}
            for x in lr_method[lr_method.find('-') + 1:].split('-'):
                split = x.split('_')
                assert len(split) == 2
                lr_method_parameters[split[0]] = float(split[1])
        else:
            lr_method_name = lr_method
            lr_method_parameters = {}

        # Compile training function
        print 'Compiling...'

        if training and not relation:
            updates = Optimization(clip=5.0).get_updates(lr_method_name, cost, params, **lr_method_parameters)
            f_train = theano.function(
                inputs=train_inputs,
                outputs=cost,
                updates=updates,
                givens=({is_train: np.cast['int32'](1)} if dropout else {})
            )
        else:
            f_train = None

        if training and relation:
            updates = Optimization(clip=5.0).get_updates(lr_method_name, relation_cost, params, **lr_method_parameters)
            r_train = theano.function(
                inputs=relation_train_inputs,
                outputs=relation_cost,
                updates=updates,
                givens=({is_train: np.cast['int32'](1)} if dropout else {})
            )
        else:
            r_train = None
        # Compile evaluation function
        if not relation:
            if not crf:
                f_eval = theano.function(
                    inputs=eval_inputs,
                    outputs=tags_scores,
                    givens=({is_train: np.cast['int32'](0)} if dropout else {})
                )
                f_eval_softmax = theano.function(
                    inputs=eval_inputs,
                    outputs=tags_scores_softmax,
                    givens=({is_train: np.cast['int32'](0)} if dropout else {})
                )
            else:
                f_eval = theano.function(
                    inputs=eval_inputs,
                    outputs=forward(observations, transitions, viterbi=True,
                                    return_alpha=False, return_best_sequence=True),
                    givens=({is_train: np.cast['int32'](0)} if dropout else {})
                )
                f_eval_softmax = theano.function(
                    inputs=eval_inputs,
                    outputs=tags_scores_softmax,
                    givens=({is_train: np.cast['int32'](0)} if dropout else {})
                )
        else:
            f_eval = None
            f_eval_softmax = None
        if relation:
            r_eval = theano.function(
                inputs=relation_eval_inputs,
                outputs=relation_scores,
                givens=({is_train: np.cast['int32'](0)} if dropout else {})
            )
        else:
            r_eval = None
            
        return f_train, f_eval, f_eval_softmax, r_train, r_eval
