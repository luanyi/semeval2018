# SemEval2018 (Under Construction)
This repository contains code and models for replicating results from the following publication:
* [The UWNLP system at SemEval-2018 Task 7: Neural Relation Extraction Model with Selectively Incorporated Concept Embeddings](SemEval, 2018)
* [Yi Luan](http://ssli.ee.washington.edu/~luanyi/), [Mari Ostendorf](https://ssli.ee.washington.edu/people/mo/), [Hannaneh Hajishirzi](https://homes.cs.washington.edu/~hannaneh/)

Part of the codebase is extended from [NER tagger](https://github.com/glample/tagger)

### Requirements
* Python 2.7, Theano and Numpy

## Getting Started
* Download word embeddings
`wget http://ssli.ee.washington.edu/tial/projects/sciIE/data/ACL_wikipedia_250_win3.mdl`
* Train Model
`./run.sh`

## Data Formats
* Keyphrase canidates need to be converted to IBO formats (data/*.ibo), followed by several features obtained from CoreNLP. For example:

`for O IN 4 prep 7 P97-1072 0`
`resolving B-Keyphrase VBG 7 pcomp 8 P97-1072.1 0`
`bridging I-Keyphrase VBG 8 xcomp 9 P97-1072.1 0`
`definite I-Keyphrase JJ 11 amod 10 P97-1072.1 0`
`descriptions I-Keyphrase NNS 9 dobj 11 P97-1072.1 0`
`. O . 1 punct 12 P97-1072 0`

The format is

`Original_word_in_text IBO_tag POS_tag headword_index dependency_relation word_id_in_the_sentence keyphrase_id sentence_id_in_the_abstract`

, deliminated by space. If the word is not a keyphrase, the keyphrase_id is replaced as its document ID.

* The relation labels (data/relation_training_reverse.txt) should be converted to

`H01-1001        3       3 4     8 7 6 4 USAGE_Reverse   keywords        information retrieval techniques`

The format is

`document_id sentence_id forward_pass_word_id backward_pass_word_id label keyword1 keyword2`

, deliminated by tab.








